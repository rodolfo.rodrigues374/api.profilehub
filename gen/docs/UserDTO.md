

# UserDTO

Data element for User
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **UUID** |  |  [optional]
**email** | **String** |  | 
**firstName** | **String** |  |  [optional]
**lastName** | **String** |  |  [optional]



