SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;


CREATE DATABASE dbcapitanigithub WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'pt_BR.utf8' LC_CTYPE = 'pt_BR.utf8';



ALTER DATABASE dbcapitanigithub OWNER TO capitani;

\connect dbcapitanigithub

CREATE TABLE t_profile (
    id_profile bigserial NOT NULL,
    login varchar(60) NOT NULL,
    avatar_url varchar(100) NOT NULL,
    node_id varchar(100) NOT NULL,
    html_url varchar(100) NOT NULL,
    followers_url varchar(100) NOT NULL,
    subscriptions_url varchar(100) NOT NULL,
    create_date timestamp NOT NULL DEFAULT now(),
    update_date timestamp NOT NULL DEFAULT now(),
    CONSTRAINT pk_profile PRIMARY KEY (id_profile)
);


CREATE SEQUENCE t_profile_seq
    INCREMENT BY 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 69
	CACHE 1
	NO CYCLE;


INSERT INTO t_profile	(login, avatar_url, node_id, html_url, followers_url, subscriptions_url, create_date, update_date) VALUES ('Teste 1', 'Teste 1', 'Teste 1', 'Teste 1', 'Teste 1', 'Teste 1', now(), now());

select * from t_profile;

