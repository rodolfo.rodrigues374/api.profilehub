FROM adoptopenjdk/openjdk11:latest
ENV TZ=America/Sao_Paulo
VOLUME /tmp
RUN ls .
ARG JAR_FILE=/target/*.jar
COPY ${JAR_FILE} app.jar
RUN ls .
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-Dspring.profiles.active=docker", "-jar", "/app.jar"]
