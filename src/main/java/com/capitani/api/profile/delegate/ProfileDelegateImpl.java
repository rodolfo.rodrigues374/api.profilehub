package com.capitani.api.profile.delegate;

import com.capitani.api.profile.ProfileApiDelegate;
import com.capitani.api.profile.service.ProfileService;
import com.capitani.dto.ProfileCreateDto;
import com.capitani.dto.ProfileListDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProfileDelegateImpl implements ProfileApiDelegate {

    private final ProfileService profileService;

    public ProfileDelegateImpl(ProfileService profileService) {
        this.profileService = profileService;
    }

    @Override
    public ResponseEntity<Void> createUser(ProfileCreateDto profileCreateDto) {
        profileService.saveProfile(profileCreateDto);
        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<List<ProfileListDto>> listUser() {
        return new ResponseEntity<List<ProfileListDto>>(profileService.listUser(), HttpStatus.OK);
    }
}
