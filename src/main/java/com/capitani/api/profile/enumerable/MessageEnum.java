package com.capitani.api.profile.enumerable;

import java.text.MessageFormat;

public enum MessageEnum {
    ProfileAlreadyExists("Profile {0} already exists database!"),
    ProfileNotFoundGitBub("Profile {0} not found GitHub!");

    private String descricao;

    MessageEnum(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    public String set(Object... arguments) {
        return MessageFormat.format(this.getDescricao(), arguments);
    }
}
