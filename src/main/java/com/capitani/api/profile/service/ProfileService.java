package com.capitani.api.profile.service;

import com.capitani.api.profile.entity.ProfileEntity;
import com.capitani.api.profile.enumerable.MessageEnum;
import com.capitani.api.profile.exeption.ConflictException;
import com.capitani.api.profile.exeption.InternalServerErrorException;
import com.capitani.api.profile.exeption.UserNotFoundException;
import com.capitani.api.profile.repository.ProfileRepository;
import com.capitani.dto.ProfileCreateDto;
import com.capitani.dto.ProfileDTO;
import com.capitani.dto.ProfileListDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import javax.validation.ValidationException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


@Service
public class ProfileService {

    @Autowired
    private ProfileRepository profileRepository;

    public ProfileCreateDto saveProfile(ProfileCreateDto profileCreateDto) {
        try {
            var resultProfile = this.profileRepository.findFirstByLogin(profileCreateDto.getLogin().toLowerCase());

            if (resultProfile.isPresent()) {
                throw new ConflictException(MessageEnum.ProfileAlreadyExists.set(profileCreateDto.getLogin()));
            }

            var result = getProfileClient(profileCreateDto.getLogin());

            ProfileEntity profileEntity = new ProfileEntity();
            profileEntity.setLogin(result.getLogin().toLowerCase());
            profileEntity.setAvatarUrl(result.getAvatarUrl());
            profileEntity.setNodeId(result.getNodeId());
            profileEntity.setHtmlUrl(result.getHtmlUrl());
            profileEntity.setFollowersUrl(result.getFollowersUrl());
            profileEntity.setSubscriptionsUrl(result.getSubscriptionsUrl());
            profileEntity.setCreateDate(LocalDateTime.now());
            profileEntity.setUpdateDate(LocalDateTime.now());

            this.profileRepository.save(profileEntity);

            return profileCreateDto;

        } catch (ConflictException e) {
            throw new ConflictException(e.getMessage());
        } catch (Exception e) {
            throw new InternalServerErrorException(e.getMessage());
        }
    }

    public List<ProfileListDto> listUser() {
        try {


            var entity = this.profileRepository.findAll();

            var profileDto = new ProfileListDto();
            var listProfileDto = new ArrayList<ProfileListDto>();

            for (ProfileEntity item : entity) {
                profileDto = new ProfileListDto();
                profileDto.setLogin(item.getLogin());
                profileDto.setAvatarUrl(item.getAvatarUrl());
                profileDto.setNodeId(item.getNodeId());
                profileDto.setHtmlUrl(item.getHtmlUrl());
                profileDto.setFollowersUrl(item.getFollowersUrl());
                profileDto.setSubscriptionsUrl(item.getSubscriptionsUrl());
                listProfileDto.add(profileDto);
            }

            return listProfileDto;

        } catch (Exception e) {
            throw new InternalServerErrorException(e.getMessage());
        }
    }

    private ProfileDTO getProfileClient(String login) {
        try {
            String url = "https://api.github.com";
            String uri = "/users/{login}";


            return WebClient
                    .create(url)
                    .get()
                    .uri(uri, login)
                    .retrieve()
                    .bodyToMono(ProfileDTO.class).block();
        } catch (
                WebClientResponseException e) {
            if (e.getStatusCode().equals(HttpStatus.NOT_FOUND))
                throw new UserNotFoundException(MessageEnum.ProfileNotFoundGitBub.set(login));
            else
                throw new ValidationException(e);
        } catch (Exception e) {
            throw new InternalServerErrorException(e.getMessage());
        }
    }
}