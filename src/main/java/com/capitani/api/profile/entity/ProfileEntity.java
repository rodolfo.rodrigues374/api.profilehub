package com.capitani.api.profile.entity;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity(name = "t_profile")
public class ProfileEntity extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tProfileSeq")
    @SequenceGenerator(name = "tProfileSeq", sequenceName = "t_profile_seq", allocationSize = 1)
    @Column(name = "id_profile")
    private Long idProfile;

    @Column(name = "login")
    private String login;

    @Column(name = "avatar_url")
    private String avatarUrl;

    @Column(name = "node_id")
    private String nodeId;

    @Column(name = "html_url")
    private String htmlUrl;

    @Column(name = "followers_url")
    private String followersUrl;

    @Column(name = "subscriptions_url")
    private String subscriptionsUrl;

}
