# Project for Spring Boot Profile GitHub
Test project for Spring Boot Profile GitHub.
The project used the following technologies.


- [ ] Swagger/OpenAPI (Used to document API end-points)
- [ ] Java 11
- [ ] Docker
- [ ] Spring Boot 2.7.7 - Maven
- [ ] OpenAPITools/openapi-generator
- [ ] Lombok (Lombok is a Java library focused on productivity and code reduction)



## Steps

1 - Execute docker compose for create images database and execute container
```
docker-compose -f docker-compose-db.yml up -d --build
```
![header][image-docker-db]


2 - Build project
```
mvn clean install
```
![header][image-mvninstall]


3 - Execute docker compose for create images project and execute container
```
docker-compose -f docker-compose.yml up -d --build
```
![header][image-docker]

 
4 - Swagger: http://localhost:8082/swagger-ui/index.html

![header][image-swagger]



[image-mvninstall]: https://gitlab.com/rodolfo.rodrigues374/api.profile.github/-/raw/main/img/mvninstall.png
[image-docker]: https://gitlab.com/rodolfo.rodrigues374/api.profile.github/-/raw/main/img/docker.png
[image-docker-db]: https://gitlab.com/rodolfo.rodrigues374/api.profile.github/-/raw/main/img/docker-db.png
[image-swagger]: https://gitlab.com/rodolfo.rodrigues374/api.profile.github/-/raw/main/img/api.png
